<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_ecom_testimonial_1');

/** MySQL database username */
define('DB_USER', 'admin');

/** MySQL database password */
define('DB_PASSWORD', 'password');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '1C_m^xhV_ux27?ZPB9+qf~OSVO@g#Trf[zE4lFV+KE*#?!%1SiYz%`u.Y/bWG~!s');
define('SECURE_AUTH_KEY',  '.jcO)af`STHDx*?,$2.kLnn{u?v{OfoSDdWAl7%cNk@n=z.0t?uAeq427y<t^]Op');
define('LOGGED_IN_KEY',    'm% qz=v]*u OyknfuEYAPi4%v(zwMp,~dYI[:^BCayid[6UC Q2+F4N=rp5p?}H)');
define('NONCE_KEY',        'Gth~Mg8-lqS5<Jd,K/A_xWv#N?p|z};iy6 O,c!NL*Q]K`eYr;<L}^! QliNb z{');
define('AUTH_SALT',        'X6eV@}>frBc;w9rhNDlg#+;Qzz0-}<U[8(8/6WU]|!YL8]zn$!<le}Bk#Gph,|>6');
define('SECURE_AUTH_SALT', 'FfPL)La$%7]=,l-b#{Eb)u7=J6-jB pcvA[0/!hyEHBWu-e9(|Wf7^!k<o{CGQe$');
define('LOGGED_IN_SALT',   '*,+lVpfNzz:>5.Ud LaNe7@I%V3Cgz8o0{hC<^7dFPYWn+@f({Ke|($rN5v]J]N*');
define('NONCE_SALT',       'R,ad3Dvd.28VBYa|MM=]e)}{ScnsE=/]CgdbUVv9oF%i[|lRK`R5(Qy/3[U!/Lae');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
